// Select tabel
const tabel = document.getElementById('pixelCanvas');
// Select color input
const color = document.getElementById('colorPicker');
let colorValue = color.value;
color.addEventListener('change', function() {
  colorValue = color.value
});
// Select size input

const height = document.getElementById('inputHeight');
let heightValue = height.value;
height.addEventListener('change', function() {
  heightValue = height.value;
});

const width = document.getElementById('inputWidth');
let widthValue = width.value;
width.addEventListener('change', function() {
  widthValue = width.value;
});

// When size is submitted by the user, call makeGrid()

document.getElementById('sizePicker').addEventListener('submit', makeGrid);

function makeGrid(e) {
  e.preventDefault();

  //clear the existing table rows
  while (tabel.hasChildNodes()) {
    tabel.removeChild(tabel.firstChild);
  }
  
  //create grid tabel
  for (let x = 0; x < heightValue; x++) {
    let rows = tabel.insertRow(x);
    for(let y = 0; y < widthValue; y++) {
      let cell = rows.insertCell(y);
      // change box color on click
      cell.addEventListener('click', function(e) {
        e.target.style.backgroundColor = colorValue;
      });
    }
  }
}
